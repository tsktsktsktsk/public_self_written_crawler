import fix_sys

import time
from Setup_tor import Setup_tor
from Url_despatcher2 import Despatch_urls
from MAIN_CONFIG import CONFIG_ARGS
from New_spider_class import SpiderBase
from Threaded_Tor2 import Threaded_Tor_fetch
from filesfortesting import url_list2 as urls
                
class Run_downloader(object):
    #I think this is running sequential still? Maybe only works properly with large batches + high amount of ports
    def __init__(self):
        self.TOR_SETUP = Setup_tor(*CONFIG_ARGS)  
                                                                                                                       
        self.TTF = Threaded_Tor_fetch(self.TOR_SETUP.build_headers(), 
                                        self.TOR_SETUP.OR_port_range(), 
                                        self.TOR_SETUP.control_port_range())
            
    def run_iteration(self, despatchObj):
        url_dict = despatchObj.build_url_dict()
        self.TTF.add_urls2queue(url_dict) #Maximum batchsize of 70-130 URLS
        threads = self.TTF.build_threads()
        return self.TTF.run_threads(threads) 
        

despatchObj = Despatch_urls(*CONFIG_ARGS)  

RD = Run_downloader()
SpiderQueue = RD.TTF.SpiderQueueDict
DespatchQueue = despatchObj.DomainQueueDict
Spider = SpiderBase(SpiderQueue, DespatchQueue)
Spider.add_urls_to_queue(urls)

print despatchObj.build_url_dict()

RD.TOR_SETUP.write_bash_script_file()
RD.TOR_SETUP.give_tor_file_permission()
print RD.TOR_SETUP.run_tor()
#RD.run_iteration(despatchObj)
"""
print torpids
for pid in torpids:
    RD.TOR_SETUP.kill_tor(pid)

print RD.TOR_SETUP.pid_dict
#"""
a = RD.TOR_SETUP

