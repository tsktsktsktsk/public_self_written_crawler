import os
import time
import copy
from fake_useragent import UserAgent #For the headers
from subprocess import call
import MAIN_CONFIG as MCONFIG


class Setup_tor(object):
    base_header = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}
        
    def __init__(self, daemon_amount, base_or_port, base_control_port, main_path):
        self.daemon_amount = daemon_amount
        self.base_or_port = base_or_port
        self.base_control_port = base_control_port
        self.UA = UserAgent()
        self.main_path = main_path
        self.pid_dict = {} #What tors are running at the moment
        self.tor_bash_script_file_path = MCONFIG.TOR_BASH_SCRIPT_FILE_NAME
        self.tor_script = MCONFIG.TOR_BASH_SCRIPT
        self.socks_str = 'SocksPort'
        self.control_str = 'ControlPort'
        
        
    #TODO: add PID - Port handler, if a tor process dies on a PID - kill it or make sure it's dead and restart a new tor process
    #TODO: make seperate bash script for single port calling (rewrite it from python?)
    #Keep list of open ports in the 9050+ range
        
    def build_headers(self):  
        headers = {}
        for i in self.OR_port_range():
            self.base_header['User-Agent'] = self.UA.random
            headers[str(i)] = copy.copy(self.base_header)
        return headers
        
    def OR_port_range(self):
        return range(self.base_or_port, self.base_or_port  + self.daemon_amount)
    
    def control_port_range(self):
        return range(self.base_control_port, self.base_control_port  + self.daemon_amount)
        
    def build_port_folders(self):
        self.build_folder(os.path.join(self.main_path, 'Ports'))
        for port_nr in self.OR_port_range():
            self.build_folder(os.path.join(self.main_path, 'Ports', str(port_nr)))
    
    def build_domain_ports_folders(self, domain):
        "Is called when a new domain enters the domain log."
        for port_nr in self.OR_port_range():
            folder_path = os.path.join(self.main_path, 'Ports', str(port_nr), domain)
            self.build_folder(folder_path)
    
    
    def get_domain_list_from_domain_folder(self):
        domain_path = os.path.join(self.main_path, 'Domains')
        domains = os.listdir(domain_path)
        return domains    
     
    def build_tor_folder(self):
       self.build_folder(os.path.join(self.main_path, 'Tor_folder'))
        
    def build_domain_folder(self):
        """Is used as a helper function for when the crawler is setup."""
        self.build_folder(os.path.join(self.main_path, 'Domains'))         
    
    def build_folder(self, path):
        try:
            os.mkdir(path)
        except OSError:
            print "Directory {} is already made".format(path)
                    
    def give_tor_file_permission(self):
        "If permission is denied, use the following command on the file: >chmod +x start_tor.sh"
        return call(['chmod', '+x', self.tor_bash_script_file_path])  
         
    def write_file(self, path, _file):
        with open(path, 'w') as f:
            f.write(_file)  
                   
    def write_bash_script_file(self, amount = None):
        if amount is None:
            amount = self.daemon_amount
        self.tor_script = self.tor_script % (self.base_or_port, self.base_control_port, (amount - 1))
        self.write_file(self.tor_bash_script_file_path, self.tor_script) #Bash for loop logic
        return "Written file succesfully."
        
    def start_tor_daemons(self):
        if os.path.isfile(self.tor_bash_script_file_path):
            call_code = call(self.tor_bash_script_file_path, shell=True)
            return call_code
        else:
            return "File not found on: {}".format(self.tor_bash_script_file_path)

    def check_if_all_running(self):
        self.get_tor_pids()
        counter = 0
        pid_control_nrs = [i[0] for i in self.pid_dict.values()]
        for control_port_nr in self.control_port_range():
            if control_port_nr in pid_control_nrs:
                counter += 1
        return counter >= self.daemon_amount 
        
    def run_tor(self):
        if self.check_if_all_running():
            return "There are more or equal amount of tor daemons running at the moment on the following pids: {}".format(str(self.pid_dict.keys()))
        else:
            self.start_tor_daemons()
            if self.check_if_all_running():
                print "Some tor instances have failed to start. Retrying." #Happens sometimes
                self.run_tor()
            return "Tor daemons started"
        
    def kill_tor(self, pid_nr):
        call(['kill', str(pid_nr)])
        del(self.pid_dict[pid_nr])
        
    def kill_all_tor(self):
        for pid in self.pid_dict:
            self.kill_tor(pid)
            
    def get_port_nr(self, word, string):
        control_index = string.index(word) + len(word) + 1
        return int(string[control_index:control_index + 4])
                
        
    def get_tor_pids(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        cmd_string = '--HashedControlPassword'
        for pid in pids:
            try:
                pid_cmdline_name = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
                if cmd_string in pid_cmdline_name:
                    control_nr = self.get_port_nr(self.control_str, pid_cmdline_name)
                    socks_nr = self.get_port_nr(self.socks_str, pid_cmdline_name)
                    self.pid_dict[pid] = (control_nr, socks_nr)
            except IOError: # proc has already terminated
                continue
                 
    def get_temp_pid_list(self):
        return self.pid_dict.keys()
        