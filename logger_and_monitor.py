import os
import logging
import MAIN_CONFIG
import pandas as pd
import numpy as np
import csv
from collections import Counter
from datetime import datetime
from uuid import UUID #Necessary for an old log entry line which didnt have str() before the writeline

class Build_logger(object):
    def __init__(self, file_name, logger_name):
        self.logger_file_path = os.path.join(MAIN_CONFIG.MAIN_PATH, "Logs")
        self.complete_file_path = os.path.join(self.logger_file_path, file_name)
        self.LoggerObj = logging.getLogger(logger_name)
        self.LoggerObj.setLevel(logging.DEBUG) 
        #LogFormatterObj = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        LogHandlerObj = logging.handlers.RotatingFileHandler(
                        self.complete_file_path, 
                        maxBytes = MAIN_CONFIG.LOGGER_FILE_SIZE, 
                        backupCount = MAIN_CONFIG.LOGGER_BACKUP_COUNT)
        if len(self.LoggerObj.handlers) == 0:
            self.LoggerObj.addHandler(LogHandlerObj) 
          

class Open_port_logs(object):
    """Opens the port logs and returns a dictionary with the domains as keys and the values as
    the logged data in the shape of a dataframe. {domain_name: domain_DataFrame, ...}"""
    #How to assert the latest file?
    #Stream the data at runtime ? (Costs?)
    #Instead of using logger as signal maybe use a different function?
    #Real time necessary? Maybe refresh every 1s?
    def __init__(self):
        self.logger_file_path = os.path.join(MAIN_CONFIG.MAIN_PATH, "Logs")
        self.logger_file_list = os.listdir(self.logger_file_path)
        
    def build_port_log_list(self):
        nested_port_nr_list = []
        for logger_file in self.logger_file_list:
            try:
                nested_port_nr_list.append([str(int(logger_file[:4])), logger_file])
            except ValueError:
                pass
        return nested_port_nr_list
        
    def build_port_log_dict(self):
        port_log_dict = {}
        port_nr_list = self.build_port_log_list()
        for port_nr, file_name in port_nr_list:
            port_log_dict[port_nr] = os.path.join(self.logger_file_path, file_name)
        return port_log_dict


    def open_csv_logfile(self, file_path):
        with open(file_path, 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='|', quotechar='|')
            file_list = []
            for row in spamreader:
                file_list.append(eval(row[0]))
            return pd.DataFrame(file_list)     
            
    def extract_domain_name(self, string):
        start = string.find('.') + 1
        end = string[start:].find('.')
        return string[start:end+start] 
        
    def get_domain_names(self, port_nr):
        port_log_dict = self.build_port_log_dict()
        port_df = self.open_csv_logfile(port_log_dict[str(port_nr)])
        domain_names = port_df['REQUESTED_URL'].apply(self.extract_domain_name)
        return domain_names, port_df
        
    def get_domain_dict(self, port_nr):
        domain_names, port_df = self.get_domain_names(port_nr)
        unique_domain_names = list(set(domain_names))
        domain_dict = {}
        for domain_name in unique_domain_names:
            domain_dict[domain_name] = port_df.loc[domain_names == domain_name]
        return domain_dict
        
    
    def merge_domain_dicts(self):
        port_nrs = [row[0] for row in self.build_port_log_list()]
        new_domain_dict = {}
        domain_list = []
        for port_nr in port_nrs:
            domain_dict = self.get_domain_dict(port_nr)
            domains = domain_dict.keys()
            for domain in domains:
                domain_list.append(domain)
                try:
                    new_domain_dict[domain] = new_domain_dict[domain].append(domain_dict[domain])
                except KeyError:
                    new_domain_dict[domain] = domain_dict[domain]
        for domain in list(set(domain_list)):
            new_domain_dict[domain].index = range(len(new_domain_dict[domain]))
        return new_domain_dict
        #return pd.DataFrame(domain_dict_list)

    def get_most_recent_batch(self, merged_domain_dict):
        most_recent_id = merged_domain_dict.loc[merged_domain_dict['DATE_OF_WRITING'].idxmax()]['BATCH_ID']
        return merged_domain_dict.loc[merged_domain_dict['BATCH_ID'] == most_recent_id]
              
  
class Helper_funcs(object):
    
    def calc_der(self, list):
        return [list[i] - list[i-1] for i in range(len(list))]

class Monitor_logs(object):
    """It is assumed that every batch_nr is unique and they follow each other in a chronological manner"""
    
    def __init__(self, merged_domain_dict):
        self.merged_domain_dict = merged_domain_dict
        self.domains = merged_domain_dict.keys()
        self.keys = merged_domain_dict[self.domains[0]]
        self.H = Helper_funcs() #Frivolous?
        
    def make_batch_dict(self):
        batch_nr_dict = {}
        for domain in self.domains:
            domain_log = self.merged_domain_dict[domain]
            batch_nrs = list(set(domain_log['BATCH_NR']))
            for batch_nr in batch_nrs:
                try:
                    batch_nr_dict[str(batch_nr)].append(self.merged_domain_dict[domain])
                except KeyError:
                    batch_nr_dict[str(batch_nr)] = self.merged_domain_dict[domain]
        return batch_nr_dict
        
            
    def make_port_domain_dict(self, domain_log_df):
        port_domain_dict = {}
        port_nrs = list(set(domain_log_df['PORT_NR']))
        for port_nr in port_nrs:
            port_domain_dict[str(port_nr)] = domain_log_df.loc[domain_log_df['PORT_NR'] == port_nr]
        return port_domain_dict       
     

    def past_times(self, batch_df, key, amount):
        batch_nrs = list(set(batch_df['BATCH_NR'].astype('int')))
        last_batch = max(batch_nrs)
        if amount <= last_batch:
            batch_nr_list = [last_batch - i for i in range(amount)]
            past_time_list = [batch_df.loc[batch_df['BATCH_NR'] == batch_nr][key].values[0] for batch_nr in batch_nr_list] #The [0] is temporary

        else:
            past_time_list = None
            
        return past_time_list
                     
    def fill_dict_times(self, df, to_fill_dict, key_list, timeframes, df_key, func):
        for key, timeframe in zip(key_list, timeframes):
            past_time_list = self.past_times(df, df_key, timeframe)
            if past_time_list is not None:
                to_fill_dict[key] = round(np.average(func(past_time_list)), 2)
        return to_fill_dict
                                                 
    def domain_statistics(self, domain):
        z = [u'BATCH_NR', u'BATCH_SIZE', u'DATE_OF_WRITING', u'FILE_SIZE', u'HEADER',
       u'OUTGOING_IP_ADRESS', u'PORT_NR', u'REQUESTED_URL', u'RESPONSE_CODE',
       u'TIME_SPEND_ON_BATCH', u'URL_RESPONSE_TIME', u'WRITTEN_TO_PATH']
        
        domain_log_df = self.merged_domain_dict[domain]
        port_domain_dict = self.make_port_domain_dict(domain_log_df)
        port_nrs = port_domain_dict.keys()
        port_domain_stats = {}
        for port_nr in port_nrs:
            port_df = port_domain_dict[port_nr]
            
            port_domain_stats[port_nr] = {}
            port_dict = port_domain_stats[port_nr]
            
            port_dict['PORT_LENGTH'] = len(port_df)
            port_dict['LAST_TIME_REQUEST'] = [port_df['DATE_OF_WRITING'].values[-1],
            datetime.fromtimestamp(int(port_df['DATE_OF_WRITING'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')]
            port_dict['AVG_REQUEST_TIME_OVERALL'] = np.average(port_df['URL_RESPONSE_TIME'])
            port_dict['RESPONSE_CODE_LIST_10'] = domain_log_df['RESPONSE_CODE'].values[-10:]
            port_dict['RESPONSE_CODE_LIST_HIST'] = Counter(domain_log_df['RESPONSE_CODE'])
            port_dict['IP_ADRESS_HIST'] = Counter(domain_log_df['OUTGOING_IP_ADRESS'])
            port_dict['FILE_SIZE_HIST'] = Counter(domain_log_df['FILE_SIZE'])
            port_dict['MOST_RECENT_FILE_SIZE_10'] = domain_log_df['FILE_SIZE'].values[-10:]
            port_dict['TOTAL_FILE_SIZE'] = sum(domain_log_df['FILE_SIZE'].values)
            port_dict['HEADER_HIST'] = Counter(domain_log_df['HEADER'])
            try:
                port_dict['MOST_RECENT_NOT_200_STATUS'] = domain_log_df.loc[domain_log_df['RESPONSE_CODE'] != 200].values[-1]
            except IndexError:
                pass
                
            TIME_FRAMES = [2, 5, 10]
            DF_KEY = 'URL_RESPONSE_TIME'
            
            STD_REQUEST_KEYS = ['STD_REQUEST_PAST_3', 'STD_REQUEST_PAST_5', 'STD_REQUEST_PAST_10']
            MEAN_REQUEST_KEYS = ['MEAN_REQUEST_PAST_3', 'MEAN_REQUEST_PAST_5', 'MEAN_REQUEST_PAST_10']
            AVG_REQUEST_KEYS = ['AVG_REQUEST_PAST_3', 'AVG_REQUEST_PAST_5', 'AVG_REQUEST_PAST_10']

            AVG_DER_REQUEST_KEYS = ['AVG_DER_REQUEST_PAST_3', 'AVG_DER_REQUEST_PAST_5', 'AVG_DER_REQUEST_PAST_10']
            AVG_MAX_REQUEST_KEYS = ['AVG_MAX_REQUEST_PAST_3', 'AVG_MAX_REQUEST_PAST_5', 'AVG_MAX_REQUEST_PAST_10']
            AVG_MIN_REQUEST_KEYS = ['AVG_MIN_REQUEST_PAST_3', 'AVG_MIN_REQUEST_PAST_5', 'AVG_MIN_REQUEST_PAST_10']

            
            port_dict = self.fill_dict_times(port_df, port_dict, AVG_REQUEST_KEYS, TIME_FRAMES, DF_KEY, np.average)
            port_dict = self.fill_dict_times(port_df, port_dict, MEAN_REQUEST_KEYS, TIME_FRAMES, DF_KEY, np.mean)
            port_dict = self.fill_dict_times(port_df, port_dict, STD_REQUEST_KEYS, TIME_FRAMES, DF_KEY, np.std)

            port_dict = self.fill_dict_times(port_df, port_dict, AVG_DER_REQUEST_KEYS, TIME_FRAMES, DF_KEY, self.H.calc_der)
            port_dict = self.fill_dict_times(port_df, port_dict, AVG_MAX_REQUEST_KEYS, TIME_FRAMES, DF_KEY, max)
            port_dict = self.fill_dict_times(port_df, port_dict, AVG_MIN_REQUEST_KEYS, TIME_FRAMES, DF_KEY, min)


        return port_domain_stats
        
    def batch_statistics(self, batch_nr):
        batch_dict = self.make_batch_dict()
        
        #Future problem with hardcoded namespaces?
        #Average derivative of the last 3, 5, 10 requests
        #Min time: Overall, previous_batch_wide, previous 10, previous 5
        #Max time  Overall, previous_batch_wide, previous 10, previous 5
        #Mean time: Overall, previous_batch_wide, previous 10, previous 5
        #Median time: Overall, previous_batch_wide, previous 10, previous 5
        #Std_time: Overall, previous_batch_wide, previous 10, previous 5
        #1st + 3rd quartile: Overall, previous_batch_wide, previous 10, previous 5
        #Amount of data: Average, min, max, quartiles, median
        pass
        
    def port_statistics(self, port_nr):
        #Get the port merged port dict??
        
        #Average derivative of the last 3, 5, 10 requests
        #Min time: Overall, previous_batch_wide, previous 10, previous 5
        #Max time  Overall, previous_batch_wide, previous 10, previous 5
        #Mean time: Overall, previous_batch_wide, previous 10, previous 5
        #Median time: Overall, previous_batch_wide, previous 10, previous 5
        #1st + 3rd quartile: Overall, previous_batch_wide, previous 10, previous 5 
        pass
        
        
    def ip_statistics(self):
        #Average derivative of the last 3, 5, 10 requests
        #Min time: Overall, previous_batch_wide, previous 10, previous 5
        #Max time  Overall, previous_batch_wide, previous 10, previous 5
        #Mean time: Overall, previous_batch_wide, previous 10, previous 5
        #Median time: Overall, previous_batch_wide, previous 10, previous 5
        #1st + 3rd quartile: Overall, previous_batch_wide, previous 10, previous 5 
        pass       

    def header_statistics(self):
        # Set of all headers
        # Count the use times
        pass
        
    def tor_statistics(self):
        #Total bandwith
        #Amount of daemon running
        #Run time per port
        #IP's used (total
        #Histogram of IP's
        #Past IP's in the past 24h
        #
        pass
        

"""
k = Open_port_logs()
merged_domain_dict = k.merge_domain_dicts()
print k.get_most_recent_batch(merged_domain_dict['nu'])


Mlog = Monitor_logs(merged_domain_dict)
domains = Mlog.domains
print domains
domain_port_batch_dict = Mlog.domain_statistics('nu')    

       

for key in domain_port_batch_dict:
    for key2 in domain_port_batch_dict[key]:
        print key2, ' '*(90 - len(str(str(domain_port_batch_dict[key][key2])))), domain_port_batch_dict[key][key2]
    print "\n"
#"""