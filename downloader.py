
import os
import sys
import copy
import time
import random
import Queue
import threading
import eventlet 
import requests as req_base #For exception catching
import uuid
from copy import copy
from stem import Signal
from stem.control import Controller
from fake_useragent import UserAgent #For the headers
from bs4 import BeautifulSoup #Used to get the IP adress
from multiprocessing import Queue as MultiQueue
from collections import defaultdict
from Logger import Build_logger
import MAIN_CONFIG  

import socks
import socket
import errno
from socket import error as socket_error
  
#TODO: 

#High prio:
#   - Integrate the cookie handling
#   - Set the request as session.
#   - rewrite the requests METHOD as a variable (now hardcoded as only GET but should have other options as well - just related to the URL)




#    Error handling?
#    How does delaying per domain per port work? Maybe do that from the url despatcher class? e.g. by skipping a batch.
#requests = eventlet.import_patched('requests')

class Threaded_Tor_fetch(object):
    """Input: Dict of headers, list of port_nrs, list of control_port_nrs.
    This is interpreted as one batch. After a random(70, 130) amount of batches a port will 
    request a new ID (including User Agent)"""
    def __init__(self, headers, port_nrs, control_port_nrs):
        #Maybe be able to edit the randomize interval?
        self.headers = headers
        self.port_nrs = port_nrs
        self.control_port_nrs = control_port_nrs
        self.queue = Queue.Queue()
        self.SpiderQueueDict = defaultdict(MultiQueue)
        self.pool = eventlet.GreenPool()
        self.UA = UserAgent()
        #Builds a dict of logger objects so that every port has their own logger.
        self.Port_LoggerObj_dict = {str(port_nr): Build_logger(
                                        str(port_nr) + MAIN_CONFIG.PORTLOGGER_FILENAME_BASE,
                                        str(port_nr) + MAIN_CONFIG.PORTLOGGER_NAME) for port_nr in port_nrs}
                                           
        self.requests = eventlet.import_patched('requests.__init__')
        self.port_new_id_thresh = {str(port_nr): random.randint(70, 130) for port_nr in port_nrs}
        self.new_id_refresh_count = {str(port_nr): 0 for port_nr in port_nrs}
        self.IP_per_port = copy(self.new_id_refresh_count)
        self.previous_batch_IP_per_port = copy(self.new_id_refresh_count)
        self.batch_nr = 0
        self.batch_unique_ID = str(uuid.uuid4())
        self.global_start = time.time()

    def make_proxy(self, port_nr):
        proxy = {'http': 'socks5://localhost:' + str(port_nr),
                'https': 'socks5://localhost:' + str(port_nr)}   
        return proxy 
        
    def port_logdict_before_req(self, url, path, port_nr, url_list, req_start_time, batch_start_time):
        logger_line_dict = {"PORT_NR":             port_nr,
                            "REQUESTED_URL":       url,
                            "WRITTEN_TO_PATH":     path,
                            "URL_RESPONSE_TIME":   round(time.time() - req_start_time, 3),
                            "OUTGOING_IP_ADRESS":  self.IP_per_port[str(port_nr)],
                            "HEADER":              self.headers[str(port_nr)]['User-Agent'], #Maybe use a counter to indicate the kind of Header as they're finite?(Less data per line)
                            "BATCH_SIZE":          len(url_list) - 1, #To account for the None end file indicator
                            "TIME_SPEND_ON_BATCH": round(time.time() - batch_start_time, 3),
                            "DATE_OF_WRITING":     int(time.time()),
                            "BATCH_NR":            self.batch_nr,
                            "BATCH_ID":            self.batch_unique_ID}
        return logger_line_dict                   
                            
    def port_logger_data(self, logger_line_dict, req, port_nr, fail = False):
        #Maybe move this function to the original Logger class to save space?
        if fail == True:
            logger_line_dict["RESPONSE_CODE"] = req #temp fix. Nohting knows it failed now
            logger_line_dict["FILE_SIZE"] = True
        else:
            logger_line_dict["RESPONSE_CODE"] = req.status_code
            logger_line_dict["FILE_SIZE"] = len(req.content)
        self.Port_LoggerObj_dict[str(port_nr)].LoggerObj.debug(str(logger_line_dict) + '|') #Escape char    
        
    def do_request(self, port_nr, control_port_nr, queue):
        #Make while true and build in method that makes it stop
        #Remove thread join.
        batch_start_time = time.time()
        url_list = queue.get()
        def do_get(url_path_tuple):
            #TODO: Build in delay per domain per port
            #TODO: Make proper IP request
            #TODO: Fix thread re-use
            req_start_time = time.time()
            url, path, domain = url_path_tuple
            fail = False
            before_req_log_dict = self.port_logdict_before_req(url, path, port_nr, url_list, req_start_time, batch_start_time)
            try:                
                get_req = self.requests.get(url,
                            proxies = self.make_proxy(port_nr),
                            headers = self.headers[str(port_nr)],
                            timeout = MAIN_CONFIG.GLOBAL_TIME_OUT) #Actual requests

            except req_base.exceptions.RequestException, e:
                get_req = e
                fail = True
                print "EROOOOOOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRR"
                self.port_logger_data(before_req_log_dict, get_req, port_nr, fail = fail)#Data for logger, Exceptions??
            return (get_req, path, url, domain)

        for req, path, url, domain in self.pool.imap(do_get, url_list):  
            if hasattr(req, 'content') == True:
                self.SpiderQueueDict[domain].put((url, req.content))
                #self.write_file(req.content, path) #Here it will fail if request is an empty response. 
            else:
                pass #No content in req?
            #End of the batch
        self.new_id_refresh_count[str(port_nr)] += 1
        if self.new_id_refresh_count[str(port_nr)] >= self.port_new_id_thresh[str(port_nr)]:
            self.rotate_ip(control_port_nr, port_nr)
        queue.task_done()
        return None
    
    def build_threads(self):            
        threads = []
        for port_nr, control_port_nr in zip(self.port_nrs, self.control_port_nrs):
            threads.append(threading.Thread(target=self.do_request, 
                                            args = (port_nr, control_port_nr, self.queue,)))
        return threads
        
    def run_threads(self, threads):
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        return True #Marks the end of the batch. This is send to the URL_Despatcher        

    def get_new_id(self, control_port_nr, port_nr):
        self.newI(control_port_nr)
        self.new_useragent(port_nr)
        self.new_id_refresh_count[str(port_nr)] = 0 #Reset the batch counter
        self.port_new_id_thresh[str(port_nr)] = random.randint(70, 130) 
        
    def rotate_ip(self, control_port_nr, port_nr):
        old_ip = self.get_ip_adress(port_nr)
        self.get_new_id(control_port_nr, port_nr)
        new_ip = self.get_ip_adress(port_nr)
        self.check_ip(new_ip, old_ip, control_port_nr, port_nr)

            
    def check_ip(self, new_ip, old_ip, control_port_nr, port_nr):
        if new_ip != old_ip and new_ip not in self.IP_per_port.values() and old_ip not in self.previous_batch_IP_per_port.values():
            self.IP_per_port[str(port_nr)] = new_ip
            self.previous_batch_IP_per_port[str(port_nr)] = old_ip
            
        else:
            self.rotate_ip(control_port_nr, port_nr)
    
    def fill_ip_port_dict(self, dict):
        for port_nr in self.port_nrs:
            dict[str(port_nr)] = self.get_ip_adress(port_nr)
        return dict     
                  
    def get_ip_adress(self, port_nr): #This isn't very nice.
        url = 'https://check.torproject.org'
        ip_req = self.requests.get(url, 
                                proxies = self.make_proxy(port_nr),
                                headers = self.headers[str(port_nr)])   
        bsObj = BeautifulSoup(ip_req.content, "lxml")
        return bsObj.strong.text           
        
    def newI(self, control_port_nr, rotate_user_agent = False):
        #Request new IP for a control_port number (8118++)
        with Controller.from_port(port = control_port_nr) as controller:
            controller.authenticate("poep") #HashedControlPassword: 16:1386B1D025289B59604B0139231A2A9490A75D87F8E63FAAFA9ABE2EBB see: https://stem.torproject.org/tutorials/the_little_relay_that_could.html
            controller.signal(Signal.NEWNYM)    
            
    def add_urls2queue(self, url_port_dict):
        """Input: a dict in the following structure:
        port_nr : [(url, file_path)]"""
        if self.batch_nr  == 0:
            self.fill_ip_port_dict(self.IP_per_port)
        for port in self.port_nrs:
            self.queue.put(url_port_dict[port])
        self.batch_nr  += 1
        self.batch_unique_ID = str(uuid.uuid4())
        #Amount of items in Queue MUST be equal to the amount of tor daemons running.
        #Too little and it doesn't end, too much and it lets it rest in the queue - leaving the impression it has completed everything.
        
    def write_file(self, data, path):
        Html_file = open(path,"w")
        Html_file.write(data)
        Html_file.close()

    def new_useragent(self, port_nr):
        self.headers[str(port_nr)]['User-Agent'] = self.UA.random
        


#"""        
url_dict = {9050: [('https://check.torproject.org/',
   '/home/casper/Desktop/CRAWLER/Ports/9050/torproject/https___check_torproject_org_.html', 'torproject'),
  ('http://www.nu.nl',
   '/home/casper/Desktop/CRAWLER/Ports/9050/nu/http___www_nu_nl.html', 'nu')],
 9051: [('https://check.torproject.org/',
   '/home/casper/Desktop/CRAWLER/Ports/9051/torproject/https___check_torproject_org_.html', 'torproject'),
  ('http://www.nu.nl',
   '/home/casper/Desktop/CRAWLER/Ports/9051/nu/http___www_nu_nl.html', 'nu')]}  
       
header = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
  'Accept-Encoding': 'none',
  'Accept-Language': 'en-US,en;q=0.8',
  'Connection': 'keep-alive',
  'User-Agent': u'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36'}
  
headers = {'9050': header,
            '9051': header}
      
Tor_daemons = 2
base_OR_port = 9050
base_control_port = 8118
main_path = '/home/casper/Desktop/CRAWLER/'

port_nrs = [9050, 9051]
control_port_nrs = [8118, 8119]
"""
TTF = Threaded_Tor_fetch(headers, port_nrs, control_port_nrs)
spiderqueuedict = TTF.SpiderQueueDict
TTF.add_urls2queue(url_dict) #Maximum batchsize of 70-130 URLS per port
TTF.add_urls2queue(url_dict) #Maximum batchsize of 70-130 URLS per port
TTF.add_urls2queue(url_dict) #Maximum batchsize of 70-130 URLS per port

threads = TTF.build_threads()
if TTF.run_threads(threads) == True:
    TTF.add_urls2queue(url_dict) #Maximum batchsize of 70-130 URLS per port
    threads = TTF.build_threads()
    TTF.run_threads(threads)
    
 



from New_spider_class import SpiderBase
SB = SpiderBase(spiderqueuedict['nu'])
#SB.do_something()

threads = TTF.build_threads()
TTF.run_threads(threads)

#"""