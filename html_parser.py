import codecs
import re
from bs4 import BeautifulSoup


class string_handler(object):
    
    def clean_string(string, illegal_characters, replacements):
        for illegal_char, replacement in zip(illegal_characters, replacements):
            string = string.replace(illegal_char, replacement)
        return string
        
    def insert_in_string(front, insertion, appendege = ''):
        return front + insertion + appendege
        
    def spaced_str_to_list(self, str_w_spaces):
        str_list = []
        space_index = [0]
        for i in range(len(str_w_spaces)-1):
            if str_w_spaces[i:i+1] == ' ':
                space_index.append(i)
        space_index.append(len(str_w_spaces))
        for i in range(len(space_index)-1):
            str_list.append(str_w_spaces[space_index[i]:space_index[i+1]].strip())
        return str_list        
        
        
class html_handler(object):
    
    def find_table(self, BsObj, table_attributes):
        object_name, attribute, value = table_attributes
        """Finds the table containing the attributes.
        Returns a BeautifulSoup object
        """
        if attribute == 'class':
            value = string_handler().spaced_str_to_list(value)#Fix necessary for BsObjects. Don't know why.
        print "BSOBJ TYPE: {}".format(type(BsObj))
        table = []
        for objectname in BsObj.find_all(object_name):
            if objectname.get(attribute) != None:
                if objectname.get(attribute) == value:
                    table = objectname
                    
        return table
    
    def filter_table(self, table, key_word = 'Tag'):
        new_table = []
        for entry in table:
            if key_word in str(type(entry)):
                new_table.append(entry)
        return new_table
        
   #####
    def table_value_list(self, table, list_attributes):
        """Returns a list with lists in it with the following make-up:
        [url-category-number, table_text, url].
        Almost always one of the two columns (url-category-number and url) is empty."""
        
        object_name, attribute = list_attributes
        tablelist = []
        try:
            for objectname in table.find_all(object_name):
                url_index_fix = objectname.get(attribute)
                table_text = objectname.get_text()
                url_link = objectname.get('href')
                tablelist.append([url_index_fix, table_text, url_link])
        except AttributeError:
            print table
            print "No table found or the arguments parsed are wrong."
            
        return tablelist     