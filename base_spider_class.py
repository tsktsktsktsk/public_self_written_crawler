
class SpiderBase(object): #Has a domain_name
    domain_name = str
    def __init__(self, SpiderQueue, DespatchQueue):
        self.SpiderQueue = SpiderQueue
        self.DespatchQueue = DespatchQueue
        self.MainUrlMethodDict = {}
        self.VisitedUrlsDict = {} #URL & LogEntry
        
    def do_something(self): #The function that calls the methods
        url, data = self.SpiderQueue[self.domain_name].get()
        return url, data
        
    def add_urls_to_queue(self, url_list): #Adds fetched urls to the Queue
        for url in url_list:
            self.DespatchQueue[self.domain_name].put(url)
    
    
