import os
#Main
MAIN_PATH = '/home/casper/Desktop/CRAWLER'

#Downloader

GLOBAL_TIME_OUT = 20

#Tor
TOR_DAEMON_AMOUNT = 2
BASE_PORT = 9050
BASE_CONTROL_PORT = 8118
CONFIG_ARGS = (TOR_DAEMON_AMOUNT,
                BASE_PORT,
                BASE_CONTROL_PORT,
                MAIN_PATH) 
                
TOR_FOLDER_PATH = os.path.join(MAIN_PATH, "Tor_folder")
TOR_BASH_SCRIPT_FILE_NAME = os.path.join(TOR_FOLDER_PATH, "start_tor.sh")

#Logs
LOGGER_FILE_SIZE = 100000 #Bytes
LOGGER_BACKUP_COUNT = 2

#Port Log - the main log to which all the metrics are written.
PORTLOGGER_FILENAME_BASE = "PortLogger.csv" #The port numbers will be appended in front.
PORTLOGGER_NAME = "PortLogger" #The port numbers will be appended in front.
PORTLOGGER_KEYS = [u'BATCH_SIZE', u'FILE_SIZE', u'HEADER', u'OUTGOING_IP_ADRESS',
                    u'PORT_NR', u'REQUESTED_URL', u'RESPONSE_CODE', u'TIME_SPEND_ON_BATCH',
                    u'URL_RESPONSE_TIME', u'WRITTEN_TO_PATH', 'DATE_OF_WRITING',
                    u'BATCH_NR']

#Domain log - This log is used to monitor the process of a given domain name and extracts a lot of it's data from the PORTLOGGER logs.
DOMAINLOGGER_FILENAME = "DomainLogger.csv"
DOMAINLOGGER_NAME = "DomainLogger"


#TOR SCRIPT
TOR_BASH_SCRIPT = """#!/bin/bash

base_socks_port=%.1d
base_control_port=%.1d

# Create data directory if it doesn't exist
if [ ! -d "data" ]; then
	mkdir "data"
fi

for i in {0..%.1d}
#for i in {0..80}

do
	j=$((i+1))
	socks_port=$((base_socks_port+i))
	control_port=$((base_control_port+i))
	if [ ! -d "data/tor$i" ]; then
		echo "Creating directory data/tor$i"
		mkdir "data/tor$i"
	fi
	# Take into account that authentication for the control port is disabled. Must be used in secure and controlled environments

	echo "Running: tor --RunAsDaemon 1 --CookieAuthentication 0 --HashedControlPassword \"\" --ControlPort $control_port --PidFile tor$i.pid --SocksPort $socks_port --DataDirectory data/tor$i"

	tor --RunAsDaemon 1 --CookieAuthentication 0 --HashedControlPassword 16:1386B1D025289B59604B0139231A2A9490A75D87F8E63FAAFA9ABE2EBB --ControlPort $control_port --PidFile tor$i.pid --SocksPort $socks_port --DataDirectory data/tor$i
done""" 

